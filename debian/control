Source: ocaml-vorbis
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Kyle Robbertze <paddatrapper@debian.org>,
           Samuel Mimram <smimram@debian.org>,
           Romain Beauxis <toots@rastageeks.org>
Build-Depends:
 cdbs (>= 0.4.72~),
 debhelper (>= 10),
 dh-buildinfo,
 ocaml-nox,
 dh-ocaml (>= 0.9),
 pkg-config,
 libvorbis-dev,
 ocaml-findlib (>= 1.2.4),
 libogg-ocaml-dev (>= 0.4.5)
Standards-Version: 4.2.1
Homepage: http://savonet.sourceforge.net/
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-vorbis.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-vorbis

Package: libvorbis-ocaml
Architecture: any
Depends: ${ocaml:Depends}, ${shlibs:Depends}, ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OCaml bindings for vorbis library
 This OCaml library interfaces the vorbis C library. It can be used to
 decode from or encode to the Ogg Vorbis compressed audio format as well
 as to get information about an Ogg Vorbis file.
 .
 Ogg Vorbis is a fully open, non-proprietary, patent-and-royalty-free,
 general-purpose compressed audio format for audio and music at fixed
 and variable bitrates from 16 to 128 kbps/channel.
 .
 This package contains only the shared runtime stub libraries.

Package: libvorbis-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends},
         libvorbis-dev, libvorbis-ocaml (= ${binary:Version}), libogg-ocaml-dev
Provides: ${ocaml:Provides}
Description: OCaml bindings for the vorbis library
 This OCaml library interfaces the vorbis C library. It can be used to
 decode from or encode to the Ogg Vorbis compressed audio format as well
 as to get information about an Ogg Vorbis file.
 .
 Ogg Vorbis is a fully open, non-proprietary, patent-and-royalty-free,
 general-purpose compressed audio format for audio and music at fixed
 and variable bitrates from 16 to 128 kbps/channel.
 .
 This package contains all the development stuff you need to use ocaml-vorbis
 in your programs.
